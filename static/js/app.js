// from data.js
var tableData = data;




function generateTable(table, data) {
  // delete all rows except header rown
  while (table.rows.length > 1) {
    table.deleteRow(1);
  }
  // build table from data.js
  for (let element of data) {
    let row = table.insertRow();
    for (key in element) {
      let cell = row.insertCell();
      let text = document.createTextNode(element[key]);
      cell.appendChild(text);
    }
  }
}
let table = document.querySelector("table");
generateTable(table, tableData);

// Event that triggers function when the button is clicked
function handleClick(){
  d3.event.preventDefault();
  let date = d3.select("#datetime").property("value");
  let filterData = tableData;

  if(date) {
      filterData = filterData.filter((row) => row.datetime === date);
  }
  generateTable(table, filterData);
}
// 'on' funtion to connect an event to the handler function
d3.selectAll("#filter-btn").on("click", handleClick);